#To run the project in localserver
Download Xampp and install in your system.
Download Gitbash.
Download and Install Composer from https://getcomposer.org/
In the command prompt type composer create-project laravel/laravel {directory} 4.2 --prefer-dist. It will install laravel project.
To run the project open gitbash from the root directory.
Type php artisan serve to start the server.
In the browser type localhost:8000/contact-form to run the project.
